> macos

Mac install tmux

```
# 墙，不能下载；复制终端代理命令
# 取消临时代理命令 unset
brew install tmux						
```



使用 [gpakosz](https://github.com/gpakosz)/**[.tmux](https://github.com/gpakosz/.tmux)** 配置文件

```
 tips:
 鼠标不能滚动
 修改配置文件 vim .tmux/.tmux.conf
 set -g mouse on
 上面设置无效：
 使用 ctrl+b [ 即可往上翻了，退出用 ctrl+c
 tmux source-file .tmux/.tmux.conf
```



> centos 7

centos 7 默认安装 1.x ，使用源码安装

```shell
wget https://github.com/libevent/libevent/releases/download/release-2.1.8-stable/libevent-2.1.8-stable.tar.gz
tar zxvf libevent-2.1.8-stable.tar.gz
cd libevent-2.1.8-stable
yum install gcc gcc++
./configure & make -j8
sudo make install

git clone https://github.com/tmux/tmux.git
cd tmux
yum install automake
sh autogen.sh
yum install -y ncurses-devel
yum install byacc flex -y
./configure && make -j8
sudo make install
cp /usr/local/lib/libevent-2.1.so.6 /lib64/libevent-2.1.so.6



# 配置
tmux new -s test
vim ~/.tmux.conf
set-option -g allow-rename off
set-window-option -g mode-keys vi
set -g prefix C-a
unbind C-b
bind C-a send-prefix
# 开启鼠标模式
set -g mouse on
# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @plugin 'tmux-plugins/tmux-continuum'

# Other examples:
# set -g @plugin 'github_username/plugin_name'
# set -g @plugin 'git@github.com/user/plugin'
# set -g @plugin 'git@bitbucket.com/user/plugin'

#tmux-resurrect
set -g @resurrect-save-bash-history 'on'
set -g @resurrect-capture-pane-contents 'on'
set -g @resurrect-strategy-vim 'session'
# set -g @resurrect-save 'S'
# set -g @resurrect-restore 'R'

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run -b '~/.tmux/plugins/tpm/tpm'



git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
tmux source ~/.tmux.conf

# 登陆到 tmux 中
tmux a -t test
使用快捷prefix + I(注意这里的I是大写)安装配置文件.tmux.conf中定义的插件了
```





复制，粘贴

`Shift`键在tmux窗口中通过鼠标文本进行复制和粘贴