**安装**

> centos 7

```
# 安装基本组件
yum install -y zsh git
wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | sh
chsh -s /usr/bin/zsh

# 安装插件
cd ~/.oh-my-zsh/custom/plugins
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git
git clone https://github.com/zsh-users/zsh-autosuggestions.git
vim ~/.zshrc
plugins=(
  git
  zsh-syntax-highlighting
  zsh-autosuggestions
)
source /root/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /root/.oh-my-zsh/custom/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source $ZSH/oh-my-zsh.sh
source ~/.zshrc

# 更换 ZSH 主题
 `~/.zshrc` 里面的  ZSH_THEME="ys"
 source ~/.zshrc
```





**主题**

修改 `~/.zshrc` 里面的  ZSH_THEME="ys"









**aws lightsai**

https://lentil1016.cn/create-aws-lightsail-instance-using-terraform/

https://zhuanlan.zhihu.com/p/50313467